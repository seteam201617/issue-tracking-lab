program DefinitionTimeToSolveTheTasks;

uses ProcessingTasks;

var quantityTasks, theShortestTask, theLongestTask:integer;
    arrayOfTheDurationsOfTasks:RealArray;
begin
  writeln('Input the quantity of tasks');
  readln(quantityTasks);
  writeln('Input the duration of each task in the format XXhYYm');
  InputTasks(arrayOfTheDurationsOfTasks, quantityTasks);
  if IsHaveTimeToPerformTasks(arrayOfTheDurationsOfTasks, quantityTasks) = true
  then writeln('The worker will have time to perform all tasks')
  else writeln('The worker will not have time to perform all tasks');
  theShortestTask := NumberOfTheShortestTask(arrayOfTheDurationsOfTasks, quantityTasks);
  writeln('Number of the shortest task: ', theShortestTask);
  writeln('The duration of the shortest task: ', arrayOfTheDurationsOfTasks[theShortestTask]:0:2);
  theLongestTask := NumberOfTheLongestTask(arrayOfTheDurationsOfTasks, quantityTasks);
  writeln('Number of the longest task: ', theLongestTask);
  writeln('The duration of the longest task: ', arrayOfTheDurationsOfTasks[theLongestTask]:0:2);
  readln;
end.

