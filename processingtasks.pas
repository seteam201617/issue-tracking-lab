unit ProcessingTasks;

interface

type RealArray =array of real;

function CheckInput(str:string):boolean;
function ChangeRowToNumber(str:string):real;
procedure InputTasks(var ArrayOfTasks :RealArray; NumberOfTasks :integer);
function IsHaveTimeToPerformTasks(arrayOfDurationsOfTasks :RealArray; numberOfTask :integer):boolean;
function NumberOfTheShortestTask(arrayOfDurationsOfTasks :RealArray; numberOfTask :integer):integer;
function NumberOfTheLongestTask(arrayOfDurationsOfTasks :RealArray; numberOfTask :integer):integer;

implementation

function CheckInput(str:string):boolean;

begin
if ( (length(str) = 6) and (str[1] >= '0') and (str[1] <= '9') and (str[2] >= '0') and (str[2] <= '9') and (str[4] >= '0') 
and (str[4] <= '9') and (str[5] >= '0') and (str[5] <= '9') and (str[3] = 'h') and (str[6] = 'm') ) then CheckInput:=true
else CheckInput:=false;
end;


function ChangeRowToNumber(str:string):real;
var
hours, minutes, error :integer;
temp :string;

begin
  temp:= copy(str,1,2);
  val(temp, hours, error);
  temp:= copy(str,4,2);
  val(temp, minutes, error);
  ChangeRowToNumber := hours + minutes/60;
end;

procedure InputTasks(var ArrayOfTasks :RealArray; NumberOfTasks :integer);
var
   Duration :string;
   i:integer;
begin
setlength(ArrayOfTasks, NumberOfTasks);
i:=0;
while i<NumberOfTasks do
begin
   begin
      write(i, ' task: ');
      readln(Duration);
      if CheckInput(Duration) then 
      ArrayOfTasks[i]:= ChangeRowToNumber(Duration)
      else 
      begin
         writeln('Incorrectly entered duration');
         writeln('Enter again');
         i-=1;
      end;
   end;
   i+=1;
end;   
end;    

function IsHaveTimeToPerformTasks(arrayOfDurationsOfTasks :RealArray; numberOfTask :integer):boolean;
const term = 40;
var i :integer;
    durationAllTasks:real;
begin
durationAllTasks := 0;
for i := 0 to numberOfTask-1 do
    durationAllTasks := durationAllTasks + arrayOfDurationsOfTasks[i];
if durationAllTasks <= term then
    IsHaveTimeToPerformTasks := true
else
    IsHaveTimeToPerformTasks := false;
end;

function NumberOfTheShortestTask(arrayOfDurationsOfTasks :RealArray; numberOfTask :integer):integer;
var i, _numberOfTheShortestTask:integer;
    durationTheShortestTask:real;
begin
durationTheShortestTask := arrayOfDurationsOfTasks[0];
_numberOfTheShortestTask := 0;
for i := 1 to numberOfTask-1 do
begin
    if arrayOfDurationsOfTasks[i] < durationTheShortestTask then
    begin
        _numberOfTheShortestTask := i;
        durationTheShortestTask := arrayOfDurationsOfTasks[i];
    end;
end;
NumberOfTheShortestTask := _numberOfTheShortestTask;
end;

function NumberOfTheLongestTask(arrayOfDurationsOfTasks :RealArray; numberOfTask :integer):integer;
var i, _numberOfTheLongestTask:integer;
    durationTheLongestTask:real;
begin
durationTheLongestTask := arrayOfDurationsOfTasks[0];
_numberOfTheLongestTask := 0;
for i := 1 to numberOfTask-1 do
begin
    if arrayOfDurationsOfTasks[i] > durationTheLongestTask then
    begin
        _numberOfTheLongestTask := i;
        durationTheLongestTask := arrayOfDurationsOfTasks[i];
    end;
end;
NumberOfTheLongestTask := _numberOfTheLongestTask;
end;

end.
